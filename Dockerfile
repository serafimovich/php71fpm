FROM composer:latest AS composer
FROM php:7.1-fpm

RUN apt-get update \
    && apt-get install -y \
        apt-utils \
        git \
        make \
        libev-dev \
        libevent-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libgmp-dev \
        libicu-dev \
        icu-devtools \
        libmcrypt-dev \
        libpq-dev \
        libtidy-dev \
        libldap2-dev \
        librecode-dev \
        libxml2-dev \
        libcurl4-openssl-dev \
        libsodium-dev \
        libzip-dev \
        libmemcached-dev \
        librabbitmq-dev \
        gettext-base \
        mariadb-client \
    && docker-php-ext-install gd \
    && docker-php-ext-install sockets \
    && docker-php-ext-install intl \
    && docker-php-ext-install bcmath\
    && docker-php-ext-install gmp \
    && docker-php-ext-install mcrypt \
    && docker-php-ext-install gettext \
    && docker-php-ext-install pdo_pgsql \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install tidy \
    && docker-php-ext-install ldap \
    && docker-php-ext-install recode \
    && docker-php-ext-install soap \
    && docker-php-ext-install curl \
    && docker-php-ext-install mysqli \
    && pecl install amqp \
    && pecl install ev \
    && pecl install event \
    && pecl install libsodium \
    && pecl install igbinary \
    && pecl install redis \
    && pecl install zip \
    && pecl install memcached \
    && docker-php-ext-enable amqp ev event sodium igbinary redis zip memcached

COPY --from=composer /usr/bin/composer /usr/bin/composer